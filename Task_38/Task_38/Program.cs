﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_38
{
    class Program
    {
        static void Main(string[] args)
        {
            double usernum = 0;
            double counter = 12;
            double i = 1;
            double ans = 0;

            Console.WriteLine("Input a number to see a multiplication table up to 12 x");

            usernum = double.Parse(Console.ReadLine());

            while (i <= counter)  
            {
                ans = usernum * i;
                Console.WriteLine($"{usernum} x {i} = {ans}");

                i++;
            }
        }
    }
}
