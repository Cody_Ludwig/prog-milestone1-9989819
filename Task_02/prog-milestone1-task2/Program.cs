﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prog_milestone1_task2
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Please tell me your date of birth (dd/mm).");
            string dob = Console.ReadLine();
            Console.WriteLine($"Thank you, your date of birth is: {dob}");
        }
    }
}
