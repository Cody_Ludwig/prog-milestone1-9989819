﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_29
{
    class Program
    {
        static void Main(string[] args)
        {
           
            string words = "The quick brown fox jumped over the fence";
            int numb = 0;
            var set = words.Split(' ');

            foreach (var word in set)
            {
                numb = numb + 1;
            }
            Console.WriteLine($"There are {numb} words in '{words}'");
        }
    }
}
