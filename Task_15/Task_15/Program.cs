﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_15
{
    class Program
    {
        static void Main(string[] args)
        {
            var numbers = new List<double> ();
            double total = 0;
            double zero = 0;
            double one = 0; 
            double two = 0;
            double three = 0;
            double four = 0;

            zero = double.Parse(Console.ReadLine());
            one = double.Parse(Console.ReadLine());
            two = double.Parse(Console.ReadLine());
            three = double.Parse(Console.ReadLine());
            four = double.Parse(Console.ReadLine());

            numbers.Add(zero);
            numbers.Add(one);
            numbers.Add(two);
            numbers.Add(three);
            numbers.Add(four);

            total = numbers[0] + numbers[1] + numbers[2] + numbers[3] + numbers[4];

            Console.WriteLine($"You added: {string.Join(",", numbers)}");
            Console.WriteLine($"Sum = {total}");
        }
    }
}
