﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_03
{
    class Program
    {
        static void Main(string[] args)
        {
            var Intro1 = "Type 1 to convert Km to Miles.";
            var Intro2 = "Type 2 to convert Miles to Km.";
            var statement1 = "Please input your distance (Km).";
            var statement2 = "Please input your distance (miles).";

            double distance = 0;
            const double kmMiles = 0.62137;
            const double milesKm = 1.62137;
           
                Console.WriteLine(Intro1);
                Console.WriteLine(Intro2);

            if (Console.ReadLine() == "1")
            {
                Console.WriteLine($"{statement1}");
                distance = int.Parse(Console.ReadLine());
                distance = distance * kmMiles;
                Console.WriteLine($"{distance} Miles");
            }
            else
            {
                Console.WriteLine($"{statement2}");
                distance = int.Parse(Console.ReadLine());
                distance = distance * milesKm;
                Console.WriteLine($"{distance}Km");
            }
        }
    }
}
