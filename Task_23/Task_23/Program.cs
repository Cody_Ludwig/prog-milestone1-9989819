﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_23
{
    class Program
    {
        static void Main(string[] args)
        {
            var weeks = new Dictionary<string, string>();
            var weekList = new List<string>();
            var endList = new List<string>();

            weeks.Add("Monday", "week");
            weeks.Add("Tuesday", "week");
            weeks.Add("Wednesday", "week");
            weeks.Add("Thurday", "week");
            weeks.Add("Friday", "week");
            weeks.Add("Saturday", "end");
            weeks.Add("Sunday", "end");
           
  
            foreach (var x in weeks)
            {
                if (x.Value == "week")
                {
                    weekList.Add(x.Key);
                }
                else
                {
                    endList.Add(x.Key);
                }
            }

           Console.WriteLine($"These days are week days: {string.Join(", ", weekList)}");
            Console.WriteLine($"And these days are weekend days: {string.Join(", ", endList)}");
        }
    }
}
