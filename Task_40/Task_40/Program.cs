﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_40
{
    class Program
    {
        static void Main(string[] args)
        {
            double monthDays = 31;
            double newtotal = 0;
            double wednesdays1 = 0;
            int wednesdays2 = 0;

            newtotal = monthDays - 2;
            wednesdays1 = newtotal / 7;
            wednesdays2 = Convert.ToInt32(wednesdays1);
          
            Console.WriteLine($"There are {wednesdays2} wednesdays in a 31 day month.");
        }
    }
}
