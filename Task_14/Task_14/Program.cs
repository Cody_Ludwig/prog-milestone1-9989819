﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_14
{
    class Program
    {
        static void Main(string[] args)
        {
            double TB = 0;
            double GB = 0;
            double Multiplied = 1024;

            Console.WriteLine("Please input storage size to convert from TB to GB");
            TB = double.Parse(Console.ReadLine());

            GB = TB * Multiplied;

            Console.WriteLine($"{TB} TB is converted to {GB} GB");
        }
    }
}
