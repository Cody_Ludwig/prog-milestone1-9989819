﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_13
{
    class Program
    {
        static void Main(string[] args)
        {
            double no1 = 0;
            double no2 = 0;
            double no3 = 0;
            double no4 = 0;
            double gst = 1.15;
            Console.WriteLine("Please input your 3 prices to see the sum of them including GST.");

            no1 = double.Parse(Console.ReadLine());
            no2 = double.Parse(Console.ReadLine());
            no3 = double.Parse(Console.ReadLine());
            no4 = (no1 + no2 + no3) * gst;

            Console.WriteLine($"The sum of the prices + gst = ${no4}");
        }
    }
}
