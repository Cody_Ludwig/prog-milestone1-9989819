﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_05
{
    class Program
    {
        static void Main(string[] args)
        {
            var time = 0;


            Console.WriteLine("Please input the hour you wish to convert from 24hr time to 12hr time (ie. 03 or 14)");
            time = int.Parse(Console.ReadLine());

            if (time < 12)
            {

                Console.WriteLine($"{time}am");

            }

            else if (time > 12)

            {

                time = time - 12;
                Console.WriteLine($"{time}pm");

            }

            else if (time == 12)

            {

                Console.WriteLine($"{time}pm");

            }

            else
            {
                Console.WriteLine("Error, please try again");
            }
        }
    }
}
