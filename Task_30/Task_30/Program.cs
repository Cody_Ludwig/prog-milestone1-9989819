﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_30
{
    class Program
    {
        static void Main(string[] args)
        {
            string stringnum1 = "0";
            string stringnum2 = "0";
            int intnum1 = 0;
            int intnum2 = 0;

            Console.WriteLine("Please input two numbers.");

            stringnum1 = Console.ReadLine();
            stringnum2 = Console.ReadLine();
            intnum1 = int.Parse(stringnum1);
            intnum2 = int.Parse(stringnum2);
            
            Console.WriteLine($"The numbers as strings added together are:");
            Console.WriteLine($"{stringnum1} + {stringnum2} = {stringnum1 + stringnum2}");
            Console.WriteLine($"The numbers as intergers added together are:");
            Console.WriteLine($"{intnum1} + {intnum2} = {intnum1 + intnum2}");



        }
    }
}
