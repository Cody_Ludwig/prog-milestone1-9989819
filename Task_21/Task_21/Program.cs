﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_21
{
    class Program
    {
        static void Main(string[] args)
        {
            var months = new Dictionary<string, string>();
            var monthsList = new List<string>();

            months.Add("January", "31");
            months.Add("February", "29");
            months.Add("March", "31");
            months.Add("April", "30");
            months.Add("May", "31");
            months.Add("June", "30");
            months.Add("July", "31");
            months.Add("August", "31");
            months.Add("September", "30");
            months.Add("October", "31");
            months.Add("November", "30");
            months.Add("December", "31");

            foreach (var x in months)
            {
                if (x.Value == "31")
                {
                    monthsList.Add(x.Key);
                }
            }

            Console.WriteLine($"Out of all the months there are {monthsList.Count} months with 31 days");
            Console.WriteLine($"These are the names of those months: {string.Join(", ", monthsList)}");
        }
    }
}
