﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_31
{
    class Program
    {
        static void Main(string[] args)
        {
            double number = 0;

            Console.WriteLine("Input a number to see if it is divisable by 3 and 4.");
            number = double.Parse(Console.ReadLine());

            if (number % 3 == 0)
            {
                if (number % 4 == 0)
                {
                    Console.WriteLine($"{number} does divide by 3 and 4 without leaving a decimal place.");
                }
                else
                {
                    Console.WriteLine($"{number} does divide by 3 but does not divide by 4 without leaving a decimal place.");
                }
            }
            else if (number % 4 == 0)
            {
                Console.WriteLine($"{number} does not divide by 3 but it does divide by 4 without leaving a decimal place.");
            }
            else
            {
                Console.WriteLine($"{number} does not divide by 3 without leaving a decimal place.");
            }     
            

        }
    }
}
