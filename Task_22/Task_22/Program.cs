﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_22
{
    class Program
    {
        static void Main(string[] args)
        {
            var food = new Dictionary<string, string>();
            var fruitList = new List<string>();
            var VegeList = new List<string>();

            food.Add("Banana", "Fruit");
            food.Add("Apple", "Fruit");
            food.Add("Cucumber", "Vegetable");
            food.Add("Lettuce", "Vegetable");
            food.Add("Tomato", "Fruit");
            food.Add("Orange", "Fruit");
            food.Add("Yam", "Vegetable");
            food.Add("Parsnip", "Vegetable");
            food.Add("Lemon", "Fruit");
            food.Add("Pineapple", "Fruit");
            food.Add("Silverbeet", "Vegetable");
            food.Add("Spinach", "Vegetable");
            food.Add("Grapes", "Fruit");

            foreach (var x in food)
            {
                if (x.Value == "Fruit")
                {
                    fruitList.Add(x.Key);
                }
                else
                {
                    VegeList.Add(x.Key);
                }
            }

            Console.WriteLine($"Theres are {fruitList.Count} fruit ");
            Console.WriteLine($"They are: {string.Join(", ", fruitList)}");
            Console.WriteLine($"Theres are {VegeList.Count} Vegetables ");
            Console.WriteLine($"They are: {string.Join(", ", VegeList)}");
        }
    }
}
